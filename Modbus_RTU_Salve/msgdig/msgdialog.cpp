#include "msgdialog.h"
#include "ui_msgdialog.h"

msgDialog::msgDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::msgDialog)
{
    ui->setupUi(this);
}

msgDialog::~msgDialog()
{
    delete ui;
}
//显示历史记录
void msgDialog::setMsg(QString msg)
{
    ui->showText->setText(msg);
}

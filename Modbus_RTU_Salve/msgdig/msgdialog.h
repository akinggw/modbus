#ifndef MSGDIALOG_H
#define MSGDIALOG_H

#include <QDialog>

namespace Ui {
class msgDialog;
}

class msgDialog : public QDialog
{
    Q_OBJECT

public:
    explicit msgDialog(QWidget *parent = 0);
    ~msgDialog();
    void setMsg(QString msg);

private:
    Ui::msgDialog *ui;
};

#endif // MSGDIALOG_H

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

/*************系统头文件*************/
#include <QMainWindow>
#include <QMessageBox>
#include <QByteArray>
#include <QDebug>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QDateTime>
#include <QFile>
#include <QFileDialog>
#include <QCloseEvent>
/***********用户头文件***************/
#include<./uartPort/uartwork.h>
#include<./uartPort/uart_type.h>
#include<./analysis/analysis_modbus.h>
#include<./utils/commont.h>
#include "./msgdig/msgdialog.h"
#define RECORD_PATH "logi/log.txt"
#define DEBUG 1

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void Init();
signals:
    void  Start(QByteArray msg,quint8 addr);
    //发送数据信号
    void sendData2Uart(QByteArray);
    //更新串口配置信号即打开串口
    void upCfg2Uart(comcfg_t);
    //关闭串口信号
    void closeUart();
    //拿取串口数据
    void uartDataProc(QByteArray);

public slots:
private slots:
    //开启串口槽
    void on_ser_concent_clicked();
    //读取数据槽
    void readFromCom(QByteArray);
    //错误消息处理槽
    void errorOccurred(QString);
    //串口标志槽
    void openSucess(bool);
    //刷新串口号槽
    void on_pushButton_clicked();
    //当前时间槽
    void timerShow();
    //回应数据帧
    void sendBackMsg(QByteArray msg);
    void wirtTablec(quint16 num,quint16 satrtaddr,QString bac);
    void wirtTabler(quint16 num,quint16 satrtaddr,QVector<quint16> bar);
    void WriteHistoricalMessage();
    void on_tabWidget_tabBarClicked(int index);
    void on_check_clicked();
    void on_check_2_clicked();
    void on_addr_textChanged(const QString &arg1);
    void on_chear_clicked();
    void ShowMsgString(QString error);
    void ShowMsgQByteArray(QByteArray ba);
private:
    Ui::MainWindow *ui;
    //串口线程对象
    uartWork *comThread;
    //串口配置结构体
    comcfg_t res_config;
    //串口开闭标志
    bool uartIsOpen;
    //串口数据缓冲
    QByteArray uartSrcData;
    //打开串口后禁止配置参数
    void uartConfigDisTrue();
    void uartConfigDisFalse();
    //文件对象
    QFile *file = nullptr;
    //当前时间定时器对象
    QTimer * m_timer;
    analysis_modbus * m_analysis;
    QTimer * m_logtimer;
    QSettings* readseting=nullptr;
    quint8 bar_indexs = 0;
    quint8 m_addr;
    //对话框窗口对象
    msgDialog * m_msg_dialog=nullptr;
    //表格初始化
    void table_dataInit();
    void ReadOutIniData(void);
    void ReadOutIniDatac(int start);
    void ReadOutIniDatar(int start);
    bool wirteQStringtoTxt();
    void closeEvent(QCloseEvent *event);
    bool judgement(QString msg);
};

#endif // MAINWINDOW_H

#-------------------------------------------------
#
# Project created by QtCreator 2021-12-23T14:38:04
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Modbus_RTU_Salve
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    uartPort/uartwork.cpp \
    utils/commont.cpp \
    analysis/analysis_modbus.cpp \
    utils/crc.cpp \
    msgdig/msgdialog.cpp

HEADERS  += mainwindow.h \
    uartPort/uart_type.h \
    uartPort/uartwork.h \
    utils/commont.h \
    analysis/analysis_modbus.h \
    utils/crc.h \
    msgdig/msgdialog.h

FORMS    += mainwindow.ui \
    msgdig/msgdialog.ui

DISTFILES +=

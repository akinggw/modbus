﻿#include "mythread.h"
#include "myserver.h"
#include "mysocket.h"
#include "mainwindow.h"

#if _MSC_VER >=1600
#pragma execution_character_set("utf-8")
#endif

MyThread::MyThread(QObject *parent)
{
    this->myserver=static_cast<MyServer*>(parent);
    this->sockethelper=nullptr;
    this->ThreadLoad = 0;
}

MyThread::~MyThread()
{
     if(this->sockethelper!=nullptr)
     {
        //将对象发送方中的信号与对象接收方中的方法断开。
        //如果连接成功断开，返回true;否则返回false。
        //断开信号槽
        sockethelper->disconnect();
        delete this->sockethelper;//释放sockethelper
     }
}

void MyThread::run()
{
    //在线程内创建对象，槽函数在这个线程中执行
    //创建一个辅助类对象
    this->sockethelper=new SocketHelper(this->myserver);
    //绑定信号槽
    connect(sockethelper,&SocketHelper::Create,sockethelper,&SocketHelper::CreateSocket);
    connect(sockethelper,&SocketHelper::AddList,myserver,&MyServer::AddInf);
    connect(sockethelper,&SocketHelper::RemoveList,myserver,&MyServer::RemoveInf);
    //消息循环
    exec();
}

SocketHelper::SocketHelper(QObject *parent)
{
    this->myserver=static_cast<MyServer*>(parent);
}

void SocketHelper::CreateSocket(qintptr socketDescriptor,int index)
{

    qDebug()<<"subThread:"<<QThread::currentThreadId();

    MySocket* tcpsocket = new MySocket(this->myserver);
    tcpsocket->setSocketOption(QAbstractSocket::LowDelayOption,0);
    tcpsocket->sockethelper = this;
    //初始化socket
    tcpsocket->setSocketDescriptor(socketDescriptor);
    //发送到UI线程记录信息
    emit AddList(tcpsocket,index);//
    if( index!= -1)//非UI线程时
    {
        //负载+1
        myserver->list_thread[index]->ThreadLoad+=1;//负载+1
        //关联释放socket,非UI线程需要阻塞
        connect(tcpsocket , &MySocket::DeleteSocket , tcpsocket, &MySocket::deal_disconnect,Qt::ConnectionType::BlockingQueuedConnection);
    }
    else
    {
       connect(tcpsocket , &MySocket::DeleteSocket , tcpsocket, &MySocket::deal_disconnect,Qt::ConnectionType::AutoConnection);
    }

    //关联显示消息
    connect(tcpsocket,&MySocket::AddMessage,myserver->mainwindow,&MainWindow::on_add_serverMessage);
    connect(tcpsocket,&MySocket::revcMsg,myserver->mainwindow,&MainWindow::revcMsgfromChile);
    //发送消息
    connect(tcpsocket,&MySocket::WriteMessage,tcpsocket,&MySocket::deal_write);
    //关联接收数据
    connect(tcpsocket , &MySocket::readyRead , tcpsocket , &MySocket::deal_readyRead);
    //关联断开连接时的处理槽
    connect(tcpsocket , &MySocket::disconnected , tcpsocket, &MySocket::deal_disconnect);

    QString ip = tcpsocket->peerAddress().toString();
    quint16 port = tcpsocket->peerPort();
    QDateTime curDateTime=QDateTime::currentDateTime();
    //发送到UI线程显示
    QString s = QString(curDateTime.toString("yyyy-MM-dd hh:mm:ss ddd")+"[%1:%2] 已连接<---").arg(ip).arg(port);
    emit tcpsocket->AddMessage(s.toUtf8());

}


﻿#ifndef MYSOCKET_H
#define MYSOCKET_H
#include<QTcpSocket>
#include<QDateTime>
#include "mythread.h"
#include"./utils/commont.h"
class MyServer;



class MySocket : public QTcpSocket
{
    Q_OBJECT
public:
    explicit MySocket(QObject *parent);
    ~MySocket();
    MyServer* m_tcpServer;
    SocketHelper* sockethelper;
public slots:
    void deal_readyRead();//读取数据槽函数
    void deal_disconnect();//断开连接槽函数
    void deal_write(QByteArray ba);//写入数据槽函数
signals:
    void AddMessage(QByteArray msg);//发送给UI显示
    void revcMsg(QByteArray msg);//接收客户端数据
    void WriteMessage(QByteArray ba);//UI发送过来数据
    void DeleteSocket();//主动关闭socket
};

#endif // MYSOCKET_H

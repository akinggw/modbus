﻿#ifndef MYTHREAD_H
#define MYTHREAD_H
#include <QThread>

//声明引用的类，使用其指针
class MyServer;
class MySocket;

//Socket创建辅助类
class SocketHelper:public QObject
{
    Q_OBJECT
public:
    explicit SocketHelper(QObject *parent = nullptr);
    MyServer* myserver;
public slots:
    //创建一个socket,传入socket通信标识符，和编号
    void CreateSocket(qintptr socketDescriptor,int index);//创建socket
signals:
    //创建信号
    void Create(qintptr socketDescriptor,int index);//创建
    //添加信号
    void AddList(MySocket* tcpsocket,int index);//添加信息
    //删除信号
    void RemoveList(MySocket* tcpsocket);//移除信息
};

//子线程类
class MyThread : public QThread
{
    Q_OBJECT
public:
    explicit  MyThread(QObject *parent);
    ~MyThread() override;
public:
    //server对象
    MyServer* myserver;
    //辅助类对象
    SocketHelper* sockethelper;
    //线程数
    int ThreadLoad;
    //运行线程
    void run() override;
};

#endif // MYTHREAD_H

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

/************系统头文件******************/
#include <QMainWindow>
#include <QMessageBox>
#include <QByteArray>
#include <QDebug>
#include <QThread>
#include <QTcpSocket>
#include<QStandardItemModel>
#include<QTableWidget>
#include<QTimer>
#include<QDateTime>
#include<QSettings>
#include<QFile>
#include<QFileInfo>
#include<QFileDialog>
#include<QTextStream>
#include <QCloseEvent>

/************用户头文件******************/
#include"./network/myserver.h"
#include"./network/mysocket.h"
#include"./network/mythread.h"
#include"./utils/commont.h"
#include "./analysis/analysis_modbus.h"
#include "./msgdig/msgdialog.h"
#define RECORD_PATH "logi/log.txt"
#define MAXADDR 65535
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void Init();
signals:
    void  Start(QByteArray msg,quint8 m_addr);
public slots:
    //显示log消息
    void on_add_serverMessage(QByteArray ba);
    //接收数据
    void revcMsgfromChile(QByteArray ba);

private slots:
    //监听连接
    void on_tcp_listen_btn_clicked();
    //回应数据帧
    void sendBackMsg(QByteArray msg);
    void timerShow();
    void wirtTablec(quint16 num,quint16 satrtaddr,QString bac);
    void wirtTabler(quint16 num, quint16 satrtaddr,QVector<quint16> bar);
    void WriteHistoricalMessage();
    void on_tabWidget_tabBarClicked(int index);
    void ShowMsgString(QString error);
    void ShowMsgQByteArray(QByteArray ba);
    void on_check_2_clicked();

    void on_refresh_ip_clicked();

    void on_addr_textChanged(const QString &arg1);

    void on_chear_clicked();

    void on_check_clicked();

private:
    Ui::MainWindow *ui;
    MyServer* m_tcpServer = nullptr;
    QTcpSocket* m_tcpSocket = nullptr;
    analysis_modbus * m_analysis;
    QThread *m_thread;
    QTimer * m_timer;
    QTimer * m_logtimer;
    QSettings* readseting=nullptr;
    quint8 bar_indexs = 0;
    QFile *file = nullptr;
    quint8 m_addr;
    //对话框窗口对象
    msgDialog * m_msg_dialog=nullptr;
    //表格初始化
    void closeEvent(QCloseEvent *event);
    void table_dataInit();
    void ReadOutIniData(void);
    void ReadOutIniDatac(int start);
    void ReadOutIniDatar(int start);
    bool wirteQStringtoTxt();
    //询问判断
    bool judgement(QString msg);
};

#endif // MAINWINDOW_H

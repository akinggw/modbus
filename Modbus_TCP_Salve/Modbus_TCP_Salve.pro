#-------------------------------------------------
#
# Project created by QtCreator 2021-12-15T09:31:26
#
#-------------------------------------------------

QT       += core gui
QT += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Modbus_TCP_Salve
TEMPLATE = app
CONFIG += c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    network/myserver.cpp \
    network/mysocket.cpp \
    network/mythread.cpp \
    utils/commont.cpp \
    analysis/analysis_modbus.cpp \
    msgdig/msgdialog.cpp

HEADERS  += mainwindow.h \
    network/myserver.h \
    network/mysocket.h \
    network/mythread.h \
    utils/commont.h \
    analysis/analysis_modbus.h \
    msgdig/msgdialog.h

FORMS    += mainwindow.ui \
    msgdig/msgdialog.ui

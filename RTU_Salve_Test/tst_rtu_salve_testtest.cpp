#include <QString>
#include <QtTest>
#include <QString>
#include <QtTest>
#include <QCoreApplication>
#include<QSettings>
#include<QDebug>
#include<QRegExp>

//测试依赖文件
#include<./utils/commont.h>
#include<./utils/crc.h>
#include<./analysis/analysis_modbus.h>
using namespace JQChecksum;
class RTU_Salve_TestTest : public QObject
{
    Q_OBJECT

public:
    RTU_Salve_TestTest();
    analysis_modbus an_msg;
    quint8 value;
private Q_SLOTS:

    //测试读取状态执行对应函数
    void test_recvModbusMsg_data();
    void test_recvModbusMsg();

};

RTU_Salve_TestTest::RTU_Salve_TestTest()
{
}

void RTU_Salve_TestTest::test_recvModbusMsg_data()
{
    //ini读取对象
    QSettings readsetings("testini/dataRTUSlave.ini",QSettings::IniFormat);
    //创建测试表
    QTest::addColumn<QByteArray>("requestMessage");
    QTest::addColumn<quint8>("expected");
    //获取测试样例数据
    quint8 testnum = readsetings.value("TestCount/count").toInt();
    //获取所有测试样例
    for(quint8 i=0;i<testnum;i++)
    {
        QString requestMessagestrings = readsetings.value("Section" + QString::number(i + 1) + "/Req").toString();
        quint8 expecteds =readsetings.value("Section" + QString::number(i + 1) + "/except").toInt();

        QByteArray requestMessage;
        HexStringToByte(requestMessagestrings,requestMessage);
        //打印日志
        qDebug() << requestMessage << expecteds;

        QString rowcount = QString::number(i);

        QTest::newRow(rowcount.toUtf8().data())
                        << requestMessage
                        << expecteds
                           ;
    }
}

void RTU_Salve_TestTest::test_recvModbusMsg()
{
    QFETCH(QByteArray, requestMessage);
    QFETCH(quint8, expected);
    //调用测试函数
    value = (quint8)an_msg.recvModbusMsg(requestMessage);
    //断言
    QCOMPARE(value, expected);
}







QTEST_APPLESS_MAIN(RTU_Salve_TestTest)

#include "tst_rtu_salve_testtest.moc"

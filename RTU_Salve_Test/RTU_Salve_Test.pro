#-------------------------------------------------
#
# Project created by QtCreator 2022-01-06T13:28:56
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_rtu_salve_testtest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tst_rtu_salve_testtest.cpp \
    analysis/analysis_modbus.cpp \
    utils/commont.cpp \
    utils/crc.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    analysis/analysis_modbus.h \
    utils/commont.h \
    utils/crc.h

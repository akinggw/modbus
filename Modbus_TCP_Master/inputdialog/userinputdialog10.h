#ifndef USERINPUTDIALOG10_H
#define USERINPUTDIALOG10_H

#include <QDialog>
#include<QByteArray>
#include<QMessageBox>
#include<QStringList>
#include<QRegExp>
#include<QTableWidgetItem>
#include<./utils/commont.h>
namespace Ui {
class userInputDialog10;
}

class userInputDialog10 : public QDialog
{
    Q_OBJECT

public:
    explicit userInputDialog10(QWidget *parent = 0);
    ~userInputDialog10();
    //获取输入信息
    QVector<quint16> infdata();
    //获取用户输入数据
    QVector<quint16> getUserInputData();
private slots:
    //设置完成
    void on_over_clicked();
    void on_start_btn_clicked();
    void on_tableregs_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);
    void on_tableregs_cellDoubleClicked(int row, int column);
    void on_check_clicked();

private:
    Ui::userInputDialog10 *ui;
    QVector<quint16> inf;
    //线圈表格初始化
    quint8 initdata[123]={0};
    QVector<quint16> bamsg;
    quint16 oldstartaddr;
    QRegExp rx;
    QString pre;
    int isfc=0;
    int isclicked=0;
    QLineEdit* edit;
    void inittable();
    void tabledatainit(quint16 BeginAddress,quint16 Number);
    void GetData0X10(QVector<quint16> &coilsDataArr,quint16 BeginAddress,quint16 Number);
};

#endif // USERINPUTDIALOG10_H

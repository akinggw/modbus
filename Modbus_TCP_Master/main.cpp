#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    qRegisterMetaType<QVector<quint16>>("QVector<quint16>");
    MainWindow w;
    w.setWindowTitle("Modbus_TCP_Master");
    w.setFixedSize(w.width(),w.height());
    w.show();
    return a.exec();
}

#-------------------------------------------------
#
# Project created by QtCreator 2021-12-15T09:29:55
#
#-------------------------------------------------

QT       += core gui
QT       += network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Modbus_TCP_Master
TEMPLATE = app
CONFIG += c++11
SOURCES += main.cpp\
        mainwindow.cpp \
    utils/commont.cpp \
    modbusmsg/modbusmsg.cpp \
    modbusmsg/msgdialog.cpp \
    inputdialog/userinput0103dialog.cpp \
    inputdialog/userinputdialog0f.cpp \
    inputdialog/userinputdialog10.cpp

INCLUDEPATH += /config/readData.ini

HEADERS  += mainwindow.h \
    utils/commont.h \
    modbusmsg/modbusmsg.h \
    modbusmsg/msgdialog.h \
    inputdialog/userinput0103dialog.h \
    inputdialog/userinputdialog0f.h \
    inputdialog/userinputdialog10.h

FORMS    += mainwindow.ui \
    modbusmsg/msgdialog.ui \
    inputdialog/userinput0103dialog.ui \
    inputdialog/userinputdialog0f.ui \
    inputdialog/userinputdialog10.ui

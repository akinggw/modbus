#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    Init();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete m_timer;
    delete m_resendtimer;
    delete mb_msg;
    delete readseting;
    delete m_TcpChile;
    //delete m_filesyn;
}

void MainWindow::Init()
{
    //显示本地ip
    QString localip = Get_LocalIp();
    ui->ip_line->setText(localip);
    //表格初始化
    table_dataInit();
    //显示当前时间
    m_timer = new QTimer(this);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(timerShow()));
    m_timer->setInterval(1000);
    m_timer->start();

    m_resendtimer = new QTimer(this);
    connect(m_resendtimer, SIGNAL(timeout()), this, SLOT(resendShow()));
    m_resendtimer->setInterval(3000);

    mb_msg = new Modbusmsg;
    connect(this,&MainWindow::TCPAnalysisMessage,mb_msg,&Modbusmsg::TCPAnalysisMessage,
            Qt::QueuedConnection);
    connect(this,&MainWindow::packageSartc,mb_msg,&Modbusmsg::packageSartc,
            Qt::QueuedConnection);
    connect(this,&MainWindow::packageSartr,mb_msg,&Modbusmsg::packageSartr,
            Qt::QueuedConnection);
    connect(mb_msg,&Modbusmsg::package_over,this,m_SendMsg);
    connect(mb_msg,&Modbusmsg::showMsgtoUi,this,showMsg);
    connect(mb_msg,&Modbusmsg::showlogmsg,this,showlogmsg);
    connect(mb_msg,&Modbusmsg::wirtTablec,this,&MainWindow::wirtTablec);
    connect(mb_msg,&Modbusmsg::wirtTabler,this,&MainWindow::wirtTabler);
    m_msg_dialog = new msgDialog(this);
    readseting = new QSettings("dataini/Data.ini", QSettings::IniFormat);
    ReadOutIniData();
    qDebug()<<"主线程:"<<QThread::currentThreadId();
}
//读取消息槽
void MainWindow::m_ReadMsg()
{
    QByteArray ba;
    ba = m_TcpChile->readAll();
    if(sendflag)
    {
        if(ba.isEmpty()==true)
        {
            qDebug() <<"没有数据！";
        }
        else
        {
            m_resendtimer->stop();
            resendNumber=0;
            emit TCPAnalysisMessage(ba);
            sendflag =false;
        }
    }
    else
    {
       ba.clear();
    }
}
//发送消息槽
void MainWindow::m_SendMsg(QByteArray ba)
{
    sendflag = true;
    resenddata = ba;
    QString s;
    m_TcpChile->write(ba);
    QDateTime current_date_time =QDateTime::currentDateTime();
    QString current_date =current_date_time.toString("yyyy.MM.dd hh:mm:ss ddd");
    ByteToHexString(s,ba);
    ui->show_Edit->append(current_date+"\n发送查询报文："+s+"\n");
    ui->show_Edit->moveCursor(QTextCursor::End);
    m_resendtimer->start();
}
//连接到服务器
void MainWindow::on_concent_btn_clicked()
{
    if(m_TcpChile == nullptr)
    {
        if(judgement("是否连接到服务器？"))
        {
            //获取本地端口
            bool ok;
            quint16 port=ui->port_line->text().toUShort(&ok);
            if(!ok)
            {
                QMessageBox::warning(this,"错误","远程端口设置有误，请重新输入");
                return;
            }
            //获取本地Ip
            QHostAddress ip(ui->ip_line->text());
            ip.toIPv4Address(&ok);
            if(!ok)
            {
                QMessageBox::warning(this,"错误","远程ip设置有误，请重新输入");
                return;
            }

            m_TcpChile=new QTcpSocket(this);
            m_TcpChile->connectToHost(ip,port);
            connect(m_TcpChile,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(ClientReadError(QAbstractSocket::SocketError)));
            //等待连接
            if (m_TcpChile->waitForConnected(500))
            {
                connect(m_TcpChile,SIGNAL(readyRead()),this,SLOT(m_ReadMsg()));

                //更新UI
                QDateTime current_date_time =QDateTime::currentDateTime();
                QString current_date =current_date_time.toString("yyyy.MM.dd hh:mm:ss ddd");
                ui->show_Edit->append(current_date+ "  已连接服务器"+QString("[%1：%2]\r\n").arg(ui->ip_line->text()).arg(ui->port_line->text()));
                ui->concent_btn->setText("断开");
                tcpconnectflag = true;
            }
            else//连接失败
            {
                QMessageBox::warning(this,"错误","连接超时或服务器未启动！");
                m_TcpChile->disconnect();
                m_TcpChile->abort();//终止
                m_TcpChile->deleteLater();
                m_TcpChile=nullptr;
                tcpconnectflag = false;
            }

        }

    }
    else
    {
        if(judgement("是否断开连接？"))
        {
            m_TcpChile->disconnect();//断开信号槽
            m_TcpChile->abort();//终止
            m_TcpChile->deleteLater();//释放
            m_TcpChile=nullptr;
            //更新UI
            QDateTime current_date_time =QDateTime::currentDateTime();
            QString current_date =current_date_time.toString("yyyy.MM.dd hh:mm:ss ddd");
            ui->show_Edit->append(current_date+ "  已断开服务器"+QString("[%1：%2]\r\n").arg(ui->ip_line->text()).arg(ui->port_line->text()));
            ui->concent_btn->setText("连接");
            tcpconnectflag = false;
        }
    }
}
//错误信息
void MainWindow::ClientReadError(QAbstractSocket::SocketError)
{
    m_TcpChile->disconnect();//断开所有信号
    m_TcpChile->abort();//终止socket连接
    m_TcpChile->deleteLater();//释放
    m_TcpChile=nullptr;
    //更新UI
    QDateTime current_date_time =QDateTime::currentDateTime();
    QString current_date =current_date_time.toString("yyyy.MM.dd hh:mm:ss ddd");
    ui->show_Edit->append(current_date+ "  已断开服务器"+QString("[%1：%2]\r\n").arg(ui->ip_line->text()).arg(ui->port_line->text()));
    ui->concent_btn->setText("连接");
}
//发送请求帧
void MainWindow::on_send_btn_clicked()
{
    if(m_TcpChile==nullptr)
    {
        QMessageBox::warning(this,"错误","还未建立连接！");
    }
    else
    {
        //调用封装类封装数据并发送
        switch(m_code)
        {
        case 1:
            if(checkUserInputData())
            {
                emit packageSartr(inf.at(0),1,inf.at(1),inf.at(2));
            }
            else
            {
                QMessageBox::warning(this,"错误","还未设置数据！");
            }
            break;
        case 3:
            if(checkUserInputData())
            {
                emit packageSartr(inf.at(0),3,inf.at(1),inf.at(2));
            }
            else
            {
                QMessageBox::warning(this,"错误","还未设置数据！");
            }
            break;
        case 15:
            if(!coildata.isEmpty())
            {
                emit packageSartc(inf.at(0),15,inf.at(1),inf.at(2),coildata,regdata);
            }
            else
            {
                QMessageBox::warning(this,"错误","还未设置数据！");
            }
            break;
        case 16:
            if(!regdata.isEmpty())
            {
                emit packageSartc(inf.at(0),16,inf.at(1),inf.at(2),coildata,regdata);
            }
            else
            {
                QMessageBox::warning(this,"错误","还未设置数据！");
            }
            break;
        default:
            QMessageBox::warning(this,"错误","还未设置数据！");
            break;
        }
    }
}
//二次检测地址，起始地址，数量用户输入数据是否为空
bool MainWindow::checkUserInputData()
{
    if(inf.isEmpty()||(inf.at(0)==0)||(inf.at(2)==0))
    {
        return false;
    }
    else
    {
        return true;
    }
}
//二次检测用户写操作数据是否为空
bool MainWindow::checkUserInputWirteData()
{
    if(!checkUserInputData())
    {
        return false;
    }
    else
    {
        return true;
    }
}
//显示回应消息
void MainWindow::showMsg(QString msg)
{
    ui->show_Edit->append(msg);
}
//显示log信息
void MainWindow::showlogmsg(QString msg)
{
    ui->show_Edit->append(msg+"\r\n");
}
//显示系统时间
void MainWindow::timerShow()
{
    QDateTime current_date_time =QDateTime::currentDateTime();
    QString current_date =current_date_time.toString("yyyy.MM.dd hh:mm:ss ddd");
    ui->timer_line->setText(current_date);
}
//超时重发
void MainWindow::resendShow()
{
    resendNumber++;
    if(resendNumber<4)
    {
        QDateTime current_date_time =QDateTime::currentDateTime();
        QString current_date =current_date_time.toString("yyyy.MM.dd hh:mm:ss ddd");
        ui->show_Edit->append(current_date+"从站无响应！");
        ui->show_Edit->append("已向目标从站第"
                              + QString::number(resendNumber)
                              + "次重新发送报文\n");
        m_TcpChile->write(resenddata);
    }
    else
    {
        resendNumber=0;
        ui->show_Edit->append("3次重传无效，从站无响应!\n");
        m_resendtimer->stop();
    }
}
//01数据设置按钮
void MainWindow::on_pushButton_clicked()
{
    m_0103dialog = new userInput0103Dialog(this);
    m_code = 0x01;
    m_0103dialog->setNumLimited("1-2000");
    int flag =  m_0103dialog->exec();
    if(!flag)
    {
        inf = m_0103dialog->infdata();
    }
    delete m_0103dialog;
}
//03数据设置按钮
void MainWindow::on_pushButton_2_clicked()
{
    m_0103dialog = new userInput0103Dialog(this);
    m_code = 0x03;
    m_0103dialog->setNumLimited("1-125");
    int flag =  m_0103dialog->exec();
    if(!flag)
    {
        inf = m_0103dialog->infdata();
    }
    delete m_0103dialog;
}
//0f数据设置按钮
void MainWindow::on_pushButton_3_clicked()
{
    m_0fdialog = new userInputDialog0f(this);
    m_code = 0x0f;
    int flag =  m_0fdialog->exec();
    if(!flag)
    {
        inf = m_0fdialog->infdata();
        coildata = m_0fdialog->getUserInputData();
    }
    delete m_0fdialog;
}
//10数据设置按钮
void MainWindow::on_pushButton_4_clicked()
{
    m_10dialog = new userInputDialog10(this);
    m_code = 0x10;
    int flag =  m_10dialog->exec();
    if(!flag)
    {
        inf = m_10dialog->infdata();
        regdata = m_10dialog->getUserInputData();
    }
    delete m_10dialog;
}
//清空窗口
void MainWindow::on_clear_btn_clicked()
{
    if(judgement("是否清除窗口？"))
    {
        ui->show_Edit->clear();
    }
}
//写消息到文件
void MainWindow::on_check_history_clicked()
{
    QString msg =ui->show_Edit->toPlainText();
    wirteQStringtoTxt(msg);
}
bool MainWindow::wirteQStringtoTxt(QString msg)
{
    QString path = QFileDialog::getOpenFileName(this,"open","./","TXT(*.txt)");
    if(path.isEmpty() == false)
    {

        QMessageBox::information(this,"文件","选择路径成功！");
        //文件对象
        file = new QFile(path);

        //打开文件
        if(file->open(QIODevice::ReadWrite |QIODevice::Text)){

            QTextStream wstream(file);
            QTextStream rstream(file);
            wstream<<msg<<"\n";
            QString showmsg;
            showmsg = rstream.readAll();
            QMessageBox::information(this,"文件","写入成功！");
            file->close();
            delete file;
            m_msg_dialog->setMsg(showmsg);
            m_msg_dialog->show();
            return true;
        }
        else
        {
            QMessageBox::information(this,"文件","失败！");
            return false;
        }

    }
    else
    {
        QMessageBox::information(this,"文件","选择路径失败！");
        return false;
    }
    file->close();
    delete file;
}
//询问判断
bool MainWindow::judgement(QString msg)
{
    QMessageBox::StandardButton close;
    close = QMessageBox::question(this,"提示",msg,QMessageBox::Yes | QMessageBox::No,QMessageBox::No);

    if(close == QMessageBox::Yes)
    {
        return true;
    }
    else
    {
        return false;
    }
}
//关闭程序事件
void MainWindow::closeEvent(QCloseEvent *event)
{
    if(judgement("确认退出程序？"))
    {
        //关闭界面，保存日志
        QString msg =ui->show_Edit->toPlainText();
        wirteQStringtoTxt(msg);
        event->accept();
    }
    else
    {
        event->ignore();
    }
}
//表格初始化
void MainWindow::table_dataInit()
{
    //设置表格
    QStringList TableHeader;
    TableHeader << "地址" << "数据";
    ui->table_coil->setHorizontalHeaderLabels(TableHeader);
    ui->table_coil->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableregs->setHorizontalHeaderLabels(TableHeader);
    ui->tableregs->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableregs->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->table_coil->setEditTriggers(QAbstractItemView::NoEditTriggers);
}
//将ini文件中的数据放到对应的表格中
void MainWindow::ReadOutIniData(void)
{
    for(int i = 0; i < (ADDRESS_MAX + 1); i++)
    {
        //地址设置
        QString adr =  "0x" + QString("%1").arg(i,4,16,QLatin1Char('0'));
        ui->table_coil->setItem(i,0, new QTableWidgetItem(QString(adr).toUpper()));
        ui->table_coil->item(i,0)->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
        ui->tableregs->setItem(i,0, new QTableWidgetItem(QString(adr)));
        ui->tableregs->item(i,0)->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
        //读出线圈数据
        QString coilData = readseting->value("Section" + QString::number(i+1) + "/coil").toString();
        //读出寄存器数据
        QString registerData = readseting->value("Section" + QString::number(i+1) + "/regi").toString();
        //在线圈数据表中显示数据
        ui->table_coil->setItem(i,1,new QTableWidgetItem(coilData));
        //设置表格内文字水平+垂直对齐
        ui->table_coil->item(i,1)->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
        //在寄存器数据表中显示数据
        ui->tableregs->setItem(i,1,new QTableWidgetItem(registerData));
        //设置表格内文字水平+垂直对齐
        ui->tableregs->item(i,1)->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
    }
}
//更新寄存器表
void MainWindow::wirtTabler(quint16 num, quint16 satrtaddr,QVector<quint16> bar)
{
    ui->tableregs->blockSignals(true);
    for(int i = satrtaddr,k=0; k < num;k++,i++)
    {
      ui->tableregs->setItem(i,1,new QTableWidgetItem(QString("%1").arg(bar[k])));
      ui->tableregs->item(i,1)->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
    }
    ui->tableregs->blockSignals(false);
    bar.clear();
}
//更新线圈表
void MainWindow::wirtTablec(quint16 num, quint16 satrtaddr,QString bac)
{
    ui->table_coil->blockSignals(true);
    for(int i = satrtaddr,k=0; k < num;k++,i++)
    {
        //在线圈数据表中显示数据
        QString data;
        if(bac[k] == '1')
        {
            data ="1";
        }
        else
        {
            data ="0";
        }
        ui->table_coil->setItem(i,1,new QTableWidgetItem(data));
        ui->table_coil->item(i,1)->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
    }
    ui->table_coil->blockSignals(false);
    bac.clear();
}
//查找
void MainWindow::on_check_clicked()
{
    int satrtaddr = ui->startaddr->text().toInt(NULL,10);
    if(bar_indexs==0)
    {
        if(satrtaddr>65535||satrtaddr<0)
        {
            QMessageBox::warning(this,"错误","起始地址超出范围，请重新输入!");
        }
        else
        {
            ReadOutIniDatac(satrtaddr);
        }
    }
    else if(bar_indexs==1)
    {
        if(satrtaddr>65535||satrtaddr<0)
        {
            QMessageBox::warning(this,"错误","起始地址超出范围，请重新输入!");
        }
        else
        {
            ReadOutIniDatar(satrtaddr);
        }
    }
}
//切换tab表格
void MainWindow::on_tabWidget_tabBarClicked(int index)
{
    switch(index){

    case 0 :bar_indexs=0;
        break;
    case 1 :bar_indexs=1;
        break;
    default:
        break;
    }
}
//定位到线圈表指定位置
void MainWindow::ReadOutIniDatac(int start)
{
    QTableWidgetItem* item = ui->table_coil->item(start,0);
    ui->table_coil->setCurrentItem(item);
    ui->table_coil->scrollToItem(item,QAbstractItemView::PositionAtCenter);
}
//定位到寄存器表指定位置
void MainWindow::ReadOutIniDatar(int start)
{
    QTableWidgetItem* item = ui->tableregs->item(start,0);
    ui->tableregs->setCurrentItem(item);
    ui->tableregs->scrollToItem(item,QAbstractItemView::PositionAtCenter);
}

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include<QMainWindow>
#include<QTcpSocket>
#include<QMessageBox>
#include<QHostAddress>
#include<QThread>
#include<QTimer>
#include<QDateTime>
#include<QFile>
#include<QFileDialog>
#include<QCloseEvent>
#include<QSettings>
#include"./utils/commont.h"
#include"./modbusmsg/modbusmsg.h"
#include"./modbusmsg/msgdialog.h"
//#include"./filesyn/filesyn.h"
#include <./inputdialog/userinput0103dialog.h>
#include <./inputdialog/userinputdialog0f.h>
#include <./inputdialog/userinputdialog10.h>
#define DEBUG 1
#define RESENDNUMBER 3        //设置超时重发次数为3次
#define WAITE_FOR_RESPONSE 3000 //设置响应报文超时时长3000ms
#define ADDRESS_MAX 65535
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void Init();
signals:
    //void synfile(QString ip);
    void packageSartr(quint8 addr, quint8 funcode, quint16 startaddr, quint16 numreg);
    void packageSartc(quint8 addr,quint8 funcode,quint16 startaddr,quint16 num,QByteArray ba,QVector<quint16>regs);
    void TCPAnalysisMessage(QByteArray ba);
public slots:
    //读取消息槽
    void m_ReadMsg();
    //发送消息槽
    void m_SendMsg(QByteArray ba);
private slots:
    void wirtTablec(quint16 num,quint16 satrtaddr,QString bac);
    void wirtTabler(quint16 num, quint16 satrtaddr,QVector<quint16> bar);
    //连接槽
    void on_concent_btn_clicked();
    //错误处理槽
    void ClientReadError(QAbstractSocket::SocketError);
    //发送按钮
    void on_send_btn_clicked();
    //当前时间槽
    //信息处理槽
    void showMsg(QString msg);
    void showlogmsg(QString msg);
    void timerShow();
    void resendShow();
    void on_pushButton_3_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_4_clicked();

    void on_clear_btn_clicked();

    void on_check_history_clicked();

   // void on_syn_btn_clicked();

    void on_check_clicked();
    void on_tabWidget_tabBarClicked(int index);

private:
    Ui::MainWindow *ui;
    QTcpSocket * m_TcpChile = nullptr;
    QTimer * m_timer;
    QTimer * m_resendtimer;
    Modbusmsg * mb_msg;
    quint8 bar_indexs = 0;
//    FileSyn * m_filesyn;
    bool tcpconnectflag=false;
    bool sendflag = false;
    QSettings* readseting=nullptr;
    //初始化超时重传次数
    int resendNumber=0;
    QFile *file = nullptr;
    //地址起始地址即数量信息
    QVector<quint16> inf;
    QByteArray coildata;
    QByteArray resenddata;
    QVector<quint16> regdata;
    //历史消息提示框
    msgDialog * m_msg_dialog=nullptr;
    //0103数据输入窗口对象
    userInput0103Dialog * m_0103dialog=nullptr;
    //0f数据窗口对象
    userInputDialog0f * m_0fdialog=nullptr;
    //10数据据窗口对象
    userInputDialog10 * m_10dialog=nullptr;
    //功能码
    quint8 m_code=0xff;
    //写入历史消息
    bool wirteQStringtoTxt(QString msg);
    //检测数据合法性
    bool checkUserInputData();
    bool checkUserInputWirteData();
    //询问判断
    bool judgement(QString msg);
    void closeEvent(QCloseEvent *event);
    void table_dataInit();
    void ReadOutIniData(void);
    void ReadOutIniDatac(int start);
    void ReadOutIniDatar(int start);
};

#endif // MAINWINDOW_H

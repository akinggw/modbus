#-------------------------------------------------
#
# Project created by QtCreator 2022-01-04T18:31:32
#
#-------------------------------------------------

QT       += testlib
QT       += core gui
QT       += network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = tst_tcp_master_testtest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tst_tcp_master_testtest.cpp \
    utils/commont.cpp \
    modbusmsg/modbusmsg.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    utils/commont.h \
    modbusmsg/modbusmsg.h

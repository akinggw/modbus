#include <QString>
#include <QtTest>
#include <QCoreApplication>
#include<QSettings>
#include<QDebug>
#include<QRegExp>

//测试依赖文件
#include<./utils/commont.h>
#include<./modbusmsg/modbusmsg.h>

class TCP_Master_TestTest : public QObject
{
    Q_OBJECT

public:
    TCP_Master_TestTest();
    Modbusmsg mb_msg;
private Q_SLOTS:

/*************************共用函数测试******************************/
    //字节反转函数测试
    void test_byteReverse_data();
    void test_byteReverse();

    //连接两个quint8数据为一个quint16数据函数测试
    void test_BondTwoUint8ToUint16_data();
    void test_BondTwoUint8ToUint16();

    //数组转十六进制字符串函数测试
    void test_HexByteArrayToHexString_data();
    void test_HexByteArrayToHexString();

/************************测试数据*************************/
    //01功能码封装函数测试
    void funCode01_data();
    //03功能码封装函数测试
    void funCode03_data();
    //0f功能码封装函数测试
    void funCode0f_data();
    //10功能码封装函数测试
    void funCode10_data();

/************************测试函数*************************/
    //01功能码封装函数
    void funCode01();
    //03功能码封装函数
    void funCode03();
    //0f功能码封装函数
    void funCode0f();
    //10功能码封装函数
    void funCode10();


/************************回应报文解析函数测试*************************/
    //报文合法性判断函数测试
    void test_MessageLegalJudge_data();
    void test_MessageLegalJudge();

    //功能码合法性判断函数测试
    void test_FuncCodeLegalJudge_data();
    void test_FuncCodeLegalJudge();

    //异常码报文处理函数测试
    void test_TCPExceptionCodeProcess_data();
    void test_TCPExceptionCodeProcess();

    //0X01功能码报文处理函数测试
    void test_TCP0X01FuncCodeProcess_data();
    void test_TCP0X01FuncCodeProcess();

    //0X03功能码报文处理函数测试
    void test_TCP0X03FuncCodeProcess_data();
    void test_TCP0X03FuncCodeProcess();

    //0X0f功能码报文处理函数测试
    void test_TCP0X0fFuncCodeProcess_data();
    void test_TCP0X0fFuncCodeProcess();

    //0X010功能码报文处理函数测试数据
    void test_TCP0X10FuncCodeProcess_data();
    void test_TCP0X10FuncCodeProcess();
};

TCP_Master_TestTest::TCP_Master_TestTest()
{

}
/*******************************start*****************************/
//01功能码封装数据
void TCP_Master_TestTest::funCode01_data()
{
    //ini读取对象
    QSettings readsetings("testini/funCode01.ini",QSettings::IniFormat);
    //创建测试表
    QTest::addColumn<quint8>("addr");
    QTest::addColumn<quint16>("startAddr");
    QTest::addColumn<quint16>("num");
    QTest::addColumn<QByteArray>("expected");
    //获取测试样例数据
    quint8 testnum = readsetings.value("Section0/num").toInt();
    //获取所有测试样例
    for(quint8 i=0;i<testnum;i++)
    {
        quint8 addrs = readsetings.value("Section" + QString::number(i + 1) + "/addr").toInt();
        quint16 startAddrs = readsetings.value("Section" + QString::number(i + 1) + "/startAddr").toInt();
        quint16 nums = readsetings.value("Section" + QString::number(i + 1) + "/num").toInt();
        QString expectedstrings = readsetings.value("Section" + QString::number(i + 1) + "/expected").toString();
        QByteArray expecteds;
        HexStringToByte(expectedstrings,expecteds);
        //打印日志
        qDebug() << addrs << startAddrs << nums << expectedstrings;

        QString rowcount = QString::number(i);

        QTest::newRow(rowcount.toUtf8().data()) << addrs
                        << startAddrs
                        << nums
                        << expecteds;
    }
}
//01功能码封装函数测试
void TCP_Master_TestTest::funCode01()
{
    QFETCH(quint8, addr);
    QFETCH(quint16, startAddr);
    QFETCH(quint16, num);
    QFETCH(QByteArray, expected);
    //调用测试函数
    mb_msg.funCode01(addr,startAddr,num);
    //断言
    QCOMPARE(mb_msg.requestMessage, expected);
}
/*******************************end******************************/

/*******************************start******************************/
//03功能码封装数据
void TCP_Master_TestTest::funCode03_data()
{
    //ini读取对象
    QSettings readsetings("testini/funCode03.ini",QSettings::IniFormat);
    //创建测试表
    QTest::addColumn<quint8>("addr");
    QTest::addColumn<quint16>("startAddr");
    QTest::addColumn<quint16>("num");
    QTest::addColumn<QByteArray>("expected");
    //获取测试样例数据
    quint8 testnum = readsetings.value("Section0/num").toInt();
    //获取所有测试样例
    for(quint8 i=0;i<testnum;i++)
    {
        quint8 addrs = readsetings.value("Section" + QString::number(i + 1) + "/addr").toInt();
        quint16 startAddrs = readsetings.value("Section" + QString::number(i + 1) + "/startAddr").toInt();
        quint16 nums = readsetings.value("Section" + QString::number(i + 1) + "/num").toInt();
        QString expectedstrings = readsetings.value("Section" + QString::number(i + 1) + "/expected").toString();
        QByteArray expecteds;
        HexStringToByte(expectedstrings,expecteds);
        //打印日志
        qDebug() << addrs << startAddrs << nums << expectedstrings;

        QString rowcount = QString::number(i);

        QTest::newRow(rowcount.toUtf8().data()) << addrs
                        << startAddrs
                        << nums
                        << expecteds;
    }
}

//03功能码封装函数测试
void TCP_Master_TestTest::funCode03()
{
    QFETCH(quint8, addr);
    QFETCH(quint16, startAddr);
    QFETCH(quint16, num);
    QFETCH(QByteArray, expected);
    //调用测试函数
    mb_msg.funCode03(addr,startAddr,num);
    //断言
    QCOMPARE(mb_msg.requestMessage, expected);
}
/*******************************end******************************/

/*******************************start*****************************/
void TCP_Master_TestTest::funCode0f_data()
{
    //ini读取对象
    QSettings readsetings("testini/funCode0f.ini",QSettings::IniFormat);
    //创建测试表
    QTest::addColumn<quint8>("addr");
    QTest::addColumn<quint16>("startAddr");
    QTest::addColumn<quint16>("num");
    QTest::addColumn<QByteArray>("coildata");
    QTest::addColumn<QByteArray>("expected");
    //获取测试样例数据
    quint8 testnum = readsetings.value("Section0/num").toInt();
    //获取所有测试样例
    for(quint8 i=0;i<testnum;i++)
    {
        quint8 addrs = readsetings.value("Section" + QString::number(i + 1) + "/addr").toInt();
        quint16 startAddrs = readsetings.value("Section" + QString::number(i + 1) + "/startAddr").toInt();
        quint16 nums = readsetings.value("Section" + QString::number(i + 1) + "/num").toInt();
        QString coildatastrings = readsetings.value("Section" + QString::number(i + 1) + "/data").toString();
        QString expectedstrings = readsetings.value("Section" + QString::number(i + 1) + "/expected").toString();
        QByteArray expecteds,coildatas;
        HexStringToByte(coildatastrings,coildatas);
        HexStringToByte(expectedstrings,expecteds);
        //打印日志
        qDebug() << addrs << startAddrs << nums << expectedstrings << coildatastrings;

        QString rowcount = QString::number(i);

        QTest::newRow(rowcount.toUtf8().data()) << addrs
                        << startAddrs
                        << nums
                        << coildatas
                        << expecteds;
    }
}
void TCP_Master_TestTest::funCode0f()
{
    QFETCH(quint8, addr);
    QFETCH(quint16, startAddr);
    QFETCH(quint16, num);
    QFETCH(QByteArray, coildata);
    QFETCH(QByteArray, expected);
    //调用测试函数
    mb_msg.funCode0f(addr,startAddr,num,coildata);
    //断言
    QCOMPARE(mb_msg.requestMessage, expected);
}
/*******************************end*****************************/


/*******************************start*****************************/
void TCP_Master_TestTest::funCode10_data()
{
    //ini读取对象
    QSettings readsetings("testini/funCode10.ini",QSettings::IniFormat);
    //创建测试表
    QTest::addColumn<quint8>("addr");
    QTest::addColumn<quint16>("startAddr");
    QTest::addColumn<QVector<quint16>>("regsdata");
    QTest::addColumn<QByteArray>("expected");
    //获取测试样例数据
    quint8 testnum = readsetings.value("Section0/num").toInt();
    //获取所有测试样例
    for(quint8 i=0;i<testnum;i++)
    {
        quint8 addrs = readsetings.value("Section" + QString::number(i + 1) + "/addr").toInt();
        quint16 startAddrs = readsetings.value("Section" + QString::number(i + 1) + "/startAddr").toInt();
        quint16 nums = readsetings.value("Section" + QString::number(i + 1) + "/num").toInt();
        QString regdatastrings = readsetings.value("Section" + QString::number(i + 1) + "/data").toString();
        QString expectedstrings = readsetings.value("Section" + QString::number(i + 1) + "/expected").toString();
        QByteArray expecteds,rdatas;
        QVector<quint16> regdatas;
        HexStringToByte(regdatastrings,rdatas);
        for(quint8 i=0;i<rdatas.size();i++)
        {
            regdatas.push_back((quint16)rdatas.at(i));
        }
        HexStringToByte(expectedstrings,expecteds);
        //打印日志
        qDebug() << addrs << startAddrs << nums << expectedstrings << regdatastrings;

        QString rowcount = QString::number(i);

        QTest::newRow(rowcount.toUtf8().data()) << addrs
                        << startAddrs
                        << regdatas
                        << expecteds;
    }
}
void TCP_Master_TestTest::funCode10()
{
    QFETCH(quint8, addr);
    QFETCH(quint16, startAddr);
    QFETCH(QVector<quint16>, regsdata);
    QFETCH(QByteArray, expected);
    //调用测试函数
    mb_msg.funCode10(addr,startAddr,regsdata);
    //断言
    QCOMPARE(mb_msg.requestMessage, expected);
}
/*******************************end*****************************/

/*******************************start*****************************/
//字节反转函数测试数据
void TCP_Master_TestTest::test_byteReverse_data()
{
    QTest::addColumn<QString>("targetString");
    QTest::addColumn<QString>("resultString");

    QTest::newRow("全0") << "00000000" << "00000000";
    QTest::newRow("01混杂") << "01110110" << "01101110";
    QTest::newRow("全1") << "11111111" << "11111111";
}
//字节反转函数测试
void TCP_Master_TestTest::test_byteReverse()
{
    QFETCH(QString, targetString);
    QFETCH(QString, resultString);

    QString resultStr = targetString;
    byteReverse(resultStr);

    QCOMPARE(resultStr, resultString);
}

//连接两个quint8数据为一个quint16数据函数测试数据
void TCP_Master_TestTest::test_BondTwoUint8ToUint16_data()
{
    QTest::addColumn<quint8>("preNum");
    QTest::addColumn<quint8>("afterNum");
    QTest::addColumn<quint16>("bondNum");

    QTest::newRow("前0后1") << quint8(0) << quint8(255) << quint16(255);
    QTest::newRow("前1后0") << quint8(255) << quint8(0) << quint16(65280);
    QTest::newRow("全1") << quint8(255) << quint8(255) << quint16(65535);
    QTest::newRow("混杂") << quint8(215) << quint8(190) << quint16(55230);
}
//连接两个quint8数据为一个quint16数据函数测试
void TCP_Master_TestTest::test_BondTwoUint8ToUint16()
{
    QFETCH(quint8, preNum);
    QFETCH(quint8, afterNum);
    QFETCH(quint16, bondNum);

    quint16 resultNum = BondTwoUint8ToUint16(preNum, afterNum);
    QCOMPARE(resultNum, bondNum);
}

//数组转十六进制字符串函数测试数据
void TCP_Master_TestTest::test_HexByteArrayToHexString_data()
{
    QTest::addColumn<QByteArray>("HexByteArr");
    QTest::addColumn<int>("ConvertLen");
    QTest::addColumn<int>("pattern");
    QTest::addColumn<QString>("expected");

    QByteArray arr1;
    arr1.resize(1);
    arr1[0] = quint8(0);
    QTest::newRow("0") << arr1 << 1 << 0 << "00";

    QByteArray arr2;
    arr2.resize(1);
    arr2[0] = quint8(255);
    QTest::newRow("1") << arr2 << 1 << 0 << "ff";

    QByteArray arr3;
    arr3.resize(2);
    arr3[0] = quint8(0);
    arr3[1] = quint8(255);
    QTest::newRow("2") << arr3 << 2 << 0 << "00ff";

    QByteArray arr4;
    arr4.resize(2);
    arr4[0] = quint8(255);
    arr4[1] = quint8(0);
    QTest::newRow("3") << arr4 << 2 << 0 << "ff00";

    QByteArray arr5;
    arr5.resize(2);
    arr5[0] = quint8(0);
    arr5[1] = quint8(255);
    QTest::newRow("4") << arr5 << 2 << 1 << "00 ff ";

    QByteArray arr6;
    arr6.resize(2);
    arr6[0] = quint8(255);
    arr6[1] = quint8(0);
    QTest::newRow("5") << arr6 << 2 << 1 << "ff 00 ";
}
//数组转十六进制字符串函数测试
void TCP_Master_TestTest::test_HexByteArrayToHexString()
{
    QFETCH(QByteArray, HexByteArr);
    QFETCH(int, ConvertLen);
    QFETCH(int, pattern);
    QFETCH(QString, expected);

    QString resultStr = HexByteArrayToHexString(HexByteArr, ConvertLen, pattern);
    QCOMPARE(resultStr, expected);
}
/*******************************end*****************************/


/*******************************start*****************************/
//报文合法性判断函数测试数据
void TCP_Master_TestTest::test_MessageLegalJudge_data()
{
    QTest::addColumn<QByteArray>("MessageArr");
    QTest::addColumn<QByteArray>("RequestMessageArr");
    QTest::addColumn<bool>("expected");

    //创建配置对象
    QSettings settings("testini/MessageLegalJudge_data.ini",QSettings::IniFormat);
    //获取样例数
    int testCount = settings.value("Section0/num").toInt();

    for(int i = 0; i < testCount; i++)
    {
        //取出样例
        QString Message = settings.value("Section" + QString::number(i + 1) + "/Message").toString();
        QString RequestMessage = settings.value("Section" + QString::number(i + 1) + "/RequestMessage").toString();
        int expectedValue = settings.value("Section" + QString::number(i + 1) + "/expected").toInt();

        //去除空格
        Message.remove(QRegExp("\\s"));
        RequestMessage.remove(QRegExp("\\s"));

        int MessageSize = (Message.size()/2);
        int RequestMessageSize = (RequestMessage.size()/2);
        bool expected;

        if(expectedValue == 1)
        {
            expected = true;
        }
        else
        {
            expected = false;
        }

        QByteArray MessageArr,RequestMessageArr;
        //转为字节数组
        for(int j = 0; j < MessageSize; j++)
        {
            MessageArr.append((quint8)(Message.mid(2*j,2).toInt(NULL,16)));
        }

        for(int j = 0; j < RequestMessageSize; j++)
        {
            RequestMessageArr.append((quint8)(RequestMessage.mid(2*j,2).toInt(NULL,16)));
        }

        QString rowNameStr = QString::number(i);
        char* rowNameCh;
        QByteArray rowNameArr = rowNameStr.toLatin1();
        rowNameCh = rowNameArr.data();
        //新建行存储
        QTest::newRow(rowNameCh) << MessageArr << RequestMessageArr << expected;
    }
}
//报文合法性判断函数测试
void TCP_Master_TestTest::test_MessageLegalJudge()
{
    QFETCH(QByteArray, MessageArr);
    QFETCH(QByteArray, RequestMessageArr);
    QFETCH(bool, expected);

    bool result = mb_msg.MessageLegalJudge(MessageArr, RequestMessageArr);

    QCOMPARE(result, expected);
}

//功能码合法性判断函数测试数据
void TCP_Master_TestTest::test_FuncCodeLegalJudge_data()
{
    QTest::addColumn<QByteArray>("MessageArr");
    QTest::addColumn<bool>("expected");

    //创建配置对象
    QSettings settings("testini/FuncCodeLegalJudge_data.ini",QSettings::IniFormat);
    //获取样例数
    int testCount = settings.value("Section0/num").toInt();

    for(int i = 0; i < testCount; i++)
    {
        //取出样例
        QString Message = settings.value("Section" + QString::number(i + 1) + "/Message").toString();
        int expectedValue = settings.value("Section" + QString::number(i + 1) + "/expected").toInt();

        //去除空格
        Message.remove(QRegExp("\\s"));
        int MessageSize = (Message.size()/2);
        bool expected;

        if(expectedValue == 1)
        {
            expected = true;
        }
        else
        {
            expected = false;
        }

        QByteArray MessageArr;
        //转为字节数组
        for(int j = 0; j < MessageSize; j++)
        {
            MessageArr.append((quint8)(Message.mid(2*j,2).toInt(NULL,16)));
        }
        QString rowNameStr = QString::number(i);
        char* rowNameCh;
        QByteArray rowNameArr = rowNameStr.toLatin1();
        rowNameCh = rowNameArr.data();
        //新建行存储
        QTest::newRow(rowNameCh) << MessageArr << expected;
    }

}
//功能码合法性判断函数测试
void TCP_Master_TestTest::test_FuncCodeLegalJudge()
{
    QFETCH(QByteArray, MessageArr);
    QFETCH(bool, expected);

    bool result = mb_msg.FuncCodeLegalJudge(MessageArr);

    QCOMPARE(result, expected);
}

//异常码报文处理函数测试数据
void TCP_Master_TestTest::test_TCPExceptionCodeProcess_data()
{
    QTest::addColumn<QByteArray>("MessageArr");
    QTest::addColumn<bool>("expected");

    //创建配置对象
    QSettings settings("testini/TCPExceptionCodeProcess_data.ini",QSettings::IniFormat);
    //获取样例数
    int testCount = settings.value("Section0/num").toInt();

    for(int i = 0; i < testCount; i++)
    {
        //取出样例
        QString Message = settings.value("Section" + QString::number(i + 1) + "/Message").toString();
        int expectedValue = settings.value("Section" + QString::number(i + 1) + "/expected").toInt();

        //去除空格
        Message.remove(QRegExp("\\s"));
        int MessageSize = (Message.size()/2);
        bool expected;

        if(expectedValue == 1)
        {
            expected = true;
        }
        else
        {
            expected = false;
        }

        QByteArray MessageArr;
        //转为字节数组
        for(int j = 0; j < MessageSize; j++)
        {
            MessageArr.append((quint8)(Message.mid(2*j,2).toInt(NULL,16)));
        }
        QString rowNameStr = QString::number(i);
        char* rowNameCh;
        QByteArray rowNameArr = rowNameStr.toLatin1();
        rowNameCh = rowNameArr.data();
        //新建行存储
        QTest::newRow(rowNameCh) << MessageArr << expected;
    }

}
//异常码报文处理函数测试
void TCP_Master_TestTest::test_TCPExceptionCodeProcess()
{
    QFETCH(QByteArray, MessageArr);
    QFETCH(bool, expected);

    bool result = mb_msg.TCPExceptionCodeProcess(MessageArr);

    QCOMPARE(result, expected);
}

//0X01功能码报文处理函数测试数据
void TCP_Master_TestTest::test_TCP0X01FuncCodeProcess_data()
{
    QTest::addColumn<QByteArray>("MessageArr");
    QTest::addColumn<QByteArray>("RequestMessageArr");
    QTest::addColumn<bool>("expected");

    //创建配置对象
    QSettings settings("testini/TCP0X01FuncCodeProcess_data.ini",QSettings::IniFormat);
    //获取样例数
    int testCount = settings.value("Section0/num").toInt();

    for(int i = 0; i < testCount; i++)
    {
        //取出样例
        QString Message = settings.value("Section" + QString::number(i + 1) + "/Message").toString();
        QString RequestMessage = settings.value("Section" + QString::number(i + 1) + "/RequestMessage").toString();
        int expectedValue = settings.value("Section" + QString::number(i + 1) + "/expected").toInt();

        //去除空格
        Message.remove(QRegExp("\\s"));
        RequestMessage.remove(QRegExp("\\s"));

        int MessageSize = (Message.size()/2);
        int RequestMessageSize = (RequestMessage.size()/2);
        bool expected;

        if(expectedValue == 1)
        {
            expected = true;
        }
        else
        {
            expected = false;
        }

        QByteArray MessageArr,RequestMessageArr;
        //转为字节数组
        for(int j = 0; j < MessageSize; j++)
        {
            MessageArr.append((quint8)(Message.mid(2*j,2).toInt(NULL,16)));
        }

        for(int j = 0; j < RequestMessageSize; j++)
        {
            RequestMessageArr.append((quint8)(RequestMessage.mid(2*j,2).toInt(NULL,16)));
        }

        QString rowNameStr = QString::number(i);
        char* rowNameCh;
        QByteArray rowNameArr = rowNameStr.toLatin1();
        rowNameCh = rowNameArr.data();
        //新建行存储
        QTest::newRow(rowNameCh) << MessageArr << RequestMessageArr << expected;
    }
}
//0X01功能码报文处理函数测试
void TCP_Master_TestTest::test_TCP0X01FuncCodeProcess()
{
    QFETCH(QByteArray, MessageArr);
    QFETCH(QByteArray, RequestMessageArr);
    QFETCH(bool, expected);

    bool result = mb_msg.TCP0X01FuncCodeProcess(MessageArr, RequestMessageArr);

    QCOMPARE(result, expected);
}

//0X03功能码报文处理函数测试数据
void TCP_Master_TestTest::test_TCP0X03FuncCodeProcess_data()
{
    QTest::addColumn<QByteArray>("MessageArr");
    QTest::addColumn<QByteArray>("RequestMessageArr");
    QTest::addColumn<bool>("expected");

    //创建配置对象
    QSettings settings("testini/TCP0X03FuncCodeProcess_data.ini",QSettings::IniFormat);
    //获取样例数
    int testCount = settings.value("Section0/num").toInt();

    for(int i = 0; i < testCount; i++)
    {
        //取出样例
        QString Message = settings.value("Section" + QString::number(i + 1) + "/Message").toString();
        QString RequestMessage = settings.value("Section" + QString::number(i + 1) + "/RequestMessage").toString();
        int expectedValue = settings.value("Section" + QString::number(i + 1) + "/expected").toInt();

        //去除空格
        Message.remove(QRegExp("\\s"));
        RequestMessage.remove(QRegExp("\\s"));

        int MessageSize = (Message.size()/2);
        int RequestMessageSize = (RequestMessage.size()/2);
        bool expected;

        if(expectedValue == 1)
        {
            expected = true;
        }
        else
        {
            expected = false;
        }

        QByteArray MessageArr,RequestMessageArr;
        //转为字节数组
        for(int j = 0; j < MessageSize; j++)
        {
            MessageArr.append((quint8)(Message.mid(2*j,2).toInt(NULL,16)));
        }

        for(int j = 0; j < RequestMessageSize; j++)
        {
            RequestMessageArr.append((quint8)(RequestMessage.mid(2*j,2).toInt(NULL,16)));
        }

        QString rowNameStr = QString::number(i);
        char* rowNameCh;
        QByteArray rowNameArr = rowNameStr.toLatin1();
        rowNameCh = rowNameArr.data();
        //新建行存储
        QTest::newRow(rowNameCh) << MessageArr << RequestMessageArr << expected;
    }
}
//0X03功能码报文处理函数测试
void TCP_Master_TestTest::test_TCP0X03FuncCodeProcess()
{
    QFETCH(QByteArray, MessageArr);
    QFETCH(QByteArray, RequestMessageArr);
    QFETCH(bool, expected);

    bool result = mb_msg.TCP0X03FuncCodeProcess(MessageArr, RequestMessageArr);

    QCOMPARE(result, expected);
}

//0X0f功能码报文处理函数测试数据
void TCP_Master_TestTest::test_TCP0X0fFuncCodeProcess_data()
{
    QTest::addColumn<QByteArray>("MessageArr");
    QTest::addColumn<QByteArray>("RequestMessageArr");
    QTest::addColumn<bool>("expected");

    //创建配置对象
    QSettings settings("testini/TCP0X0fFuncCodeProcess_data.ini",QSettings::IniFormat);
    //获取样例数
    int testCount = settings.value("Section0/num").toInt();

    for(int i = 0; i < testCount; i++)
    {
        //取出样例
        QString Message = settings.value("Section" + QString::number(i + 1) + "/Message").toString();
        QString RequestMessage = settings.value("Section" + QString::number(i + 1) + "/RequestMessage").toString();
        int expectedValue = settings.value("Section" + QString::number(i + 1) + "/expected").toInt();

        //去除空格
        Message.remove(QRegExp("\\s"));
        RequestMessage.remove(QRegExp("\\s"));

        int MessageSize = (Message.size()/2);
        int RequestMessageSize = (RequestMessage.size()/2);
        bool expected;

        if(expectedValue == 1)
        {
            expected = true;
        }
        else
        {
            expected = false;
        }

        QByteArray MessageArr,RequestMessageArr;
        //转为字节数组
        for(int j = 0; j < MessageSize; j++)
        {
            MessageArr.append((quint8)(Message.mid(2*j,2).toInt(NULL,16)));
        }

        for(int j = 0; j < RequestMessageSize; j++)
        {
            RequestMessageArr.append((quint8)(RequestMessage.mid(2*j,2).toInt(NULL,16)));
        }

        QString rowNameStr = QString::number(i);
        char* rowNameCh;
        QByteArray rowNameArr = rowNameStr.toLatin1();
        rowNameCh = rowNameArr.data();
        //新建行存储
        QTest::newRow(rowNameCh) << MessageArr << RequestMessageArr << expected;
    }
}
//0X0f功能码报文处理函数测试
void TCP_Master_TestTest::test_TCP0X0fFuncCodeProcess()
{
    QFETCH(QByteArray, MessageArr);
    QFETCH(QByteArray, RequestMessageArr);
    QFETCH(bool, expected);

    bool result = mb_msg.TCP0X0fFuncCodeProcess(MessageArr, RequestMessageArr);

    QCOMPARE(result, expected);
}

//0X010功能码报文处理函数测试数据
void TCP_Master_TestTest::test_TCP0X10FuncCodeProcess_data()
{
    QTest::addColumn<QByteArray>("MessageArr");
    QTest::addColumn<QByteArray>("RequestMessageArr");
    QTest::addColumn<bool>("expected");

    //创建配置对象
    QSettings settings("testini/TCP0X10FuncCodeProcess_data.ini",QSettings::IniFormat);
    //获取样例数
    int testCount = settings.value("Section0/num").toInt();

    for(int i = 0; i < testCount; i++)
    {
        //取出样例
        QString Message = settings.value("Section" + QString::number(i + 1) + "/Message").toString();
        QString RequestMessage = settings.value("Section" + QString::number(i + 1) + "/RequestMessage").toString();
        int expectedValue = settings.value("Section" + QString::number(i + 1) + "/expected").toInt();

        //去除空格
        Message.remove(QRegExp("\\s"));
        RequestMessage.remove(QRegExp("\\s"));

        int MessageSize = (Message.size()/2);
        int RequestMessageSize = (RequestMessage.size()/2);
        bool expected;

        if(expectedValue == 1)
        {
            expected = true;
        }
        else
        {
            expected = false;
        }

        QByteArray MessageArr,RequestMessageArr;
        //转为字节数组
        for(int j = 0; j < MessageSize; j++)
        {
            MessageArr.append((quint8)(Message.mid(2*j,2).toInt(NULL,16)));
        }

        for(int j = 0; j < RequestMessageSize; j++)
        {
            RequestMessageArr.append((quint8)(RequestMessage.mid(2*j,2).toInt(NULL,16)));
        }

        QString rowNameStr = QString::number(i);
        char* rowNameCh;
        QByteArray rowNameArr = rowNameStr.toLatin1();
        rowNameCh = rowNameArr.data();
        //新建行存储
        QTest::newRow(rowNameCh) << MessageArr << RequestMessageArr << expected;
    }
}
//0X10功能码报文处理函数测试
void TCP_Master_TestTest::test_TCP0X10FuncCodeProcess()
{
    QFETCH(QByteArray, MessageArr);
    QFETCH(QByteArray, RequestMessageArr);
    QFETCH(bool, expected);

    bool result = mb_msg.TCP0X10FuncCodeProcess(MessageArr, RequestMessageArr);

    QCOMPARE(result, expected);
}
/*******************************end*****************************/





QTEST_MAIN(TCP_Master_TestTest)

#include "tst_tcp_master_testtest.moc"

#ifndef MODBUSMSG_H
#define MODBUSMSG_H

#include <QObject>
#include<QDebug>
#include<QMessageBox>
#include<QSettings>
#include<QThread>
#include"./utils/commont.h"
//数据长度
#define MAXDATA                256
//MODBUS协议标识符
#define MB_PROTOCOL            0x0000


class Modbusmsg : public QObject
{
    Q_OBJECT
public:
    explicit Modbusmsg();
    ~Modbusmsg();

    //事务处理标识符
    quint16 mb_identifier=1;
    quint8 mb_lenght=0;
    quint8 msgbuf[MAXDATA];
    QVector<quint16> bar;
    QString coildata;
    //初始化请求报文数组
    QByteArray requestMessage;
    //MBAP事务元标识符
    quint16 transactionIdentifier = 0;
    QSettings* readsetings=nullptr;
    QThread *m_thread;
    //报文封装函数
    //01功能码封装函数
    void funCode01(quint8 addr,quint16 startaddr,quint16 numreg);
    //03功能码封装函数
    void funCode03(quint8 addr,quint16 startaddr,quint16 numreg);
    //0f功能码封装函数
    void funCode0f(quint8 addr,quint16 startaddr,quint16 num,QByteArray ba);
    //10功能码封装函数
    void funCode10(quint8 addr,quint16 startaddr,QVector<quint16> addrvalue);

/********************************解析响应报文start**********************************/
    //报文合法性判断函数
    bool MessageLegalJudge(QByteArray MessageArr, QByteArray requestMessageArr);
    // 功能码合法性判断函数
    bool FuncCodeLegalJudge(QByteArray MessageArr);
    //异常码报文处理函数
    bool TCPExceptionCodeProcess(QByteArray MessageArr);
    // 异常码判断函数
    void TCPExceptionCodeJudge(quint8 excCode);
    //0X01功能码报文处理函数
    bool TCP0X01FuncCodeProcess(QByteArray MessageArr, QByteArray requestMessageArr);
    //0X03功能码报文处理函数
    bool TCP0X03FuncCodeProcess(QByteArray MessageArr, QByteArray requestMessageArr);
    //0X0f功能码报文处理函数
    bool TCP0X0fFuncCodeProcess(QByteArray MessageArr, QByteArray requestMessageArr);
    //0X10功能码报文处理函数
    bool TCP0X10FuncCodeProcess(QByteArray MessageArr, QByteArray requestMessageArr);
    void WirteRegsDataToIni(quint16 startaddr,quint16 num,QVector<quint16> ba);
    void WirteCoilDataToIni(quint16 satrt, QString CoilData);
    /********************************解析响应报文end**********************************/
    //0f10报文处理
    void packageSartc(quint8 addr,quint8 funcode,quint16 startaddr,quint16 num,QByteArray ba,QVector<quint16>regs);
    //0103报文处理
    void packageSartr(quint8 addr,quint8 funcode,quint16 startaddr,quint16 numreg);
    //解析响应报文
    bool TCPAnalysisMessage(QByteArray MessageArray);
signals:
    void package_over(QByteArray package_msg);
    void showMsgtoUi(QString);
    void showlogmsg(QString);
    void wirtTablec(quint16 num, quint16 satrtaddr,QString bac);
    void wirtTabler(quint16 num, quint16 satrtaddr,QVector<quint16> ba);
public slots:
private:

};

#endif // MODBUSMSG_H

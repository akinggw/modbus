#ifndef MAINWINDOW_H
#define MAINWINDOW_H

/*************系统头文件*************/
#include <QMainWindow>
#include <QMessageBox>
#include <QByteArray>
#include <QDebug>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QDateTime>
#include <QFile>
#include <QFileDialog>
#include<QThread>
#include<QCloseEvent>
#include<QSettings>
/***********用户头文件***************/
#include<./uartPort/uartwork.h>
#include<./uartPort/uart_type.h>
#include<./utils/commont.h>
#include<./modbusmsg/modbusmsg.h>
#include "./modbusmsg/msgdialog.h"
#include <./inputdialog/userinput0103dialog.h>
#include <./inputdialog/userinputdialog0f.h>
#include <./inputdialog/userinputdialog10.h>
#define DEBUG 1
#define RESENDNUMBER 3        //设置超时重发次数为3次
#define WAITE_FOR_RESPONSE 3000 //设置响应报文超时时长3000ms
#define ADDRESS_MAX 65535
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void Init();

signals:
    //发送数据信号
    void sendData2Uart(QByteArray);
    //更新串口配置信号即打开串口
    void upCfg2Uart(comcfg_t);
    //关闭串口信号
    void closeUart();
    //拿取串口数据
    void uartDataProc(QByteArray);

    void packageSartr(quint8 addr, quint8 funcode, quint16 startaddr, quint16 numreg);
    void packageSartc(quint8 addr,quint8 funcode,quint16 startaddr,quint16 num,QByteArray ba,QVector<quint16>regs);
    void ParseResponseMessage(QByteArray ba);
public slots:
private slots:
    void wirtTablec(quint16 num,quint16 satrtaddr,QString bac);
    void wirtTabler(quint16 num, quint16 satrtaddr,QVector<quint16> bar);
    //开启串口槽
    void on_ser_concent_clicked();
    //读取数据槽
    void readFromCom(QByteArray);
    //错误消息处理槽
    void errorOccurred(QString);
    //串口标志槽
    void openSucess(bool);
    //刷新串口号槽
    void on_pushButton_clicked();
    void m_SendMsg(QByteArray ba);
    //消息发送槽函数
    void on_send_msg_clicked();
    //信息处理槽
    void showMsg(QString msg);
    void showlogmsg(QString msg);
    //当前时间槽
    void timerShow();
    void resendShow();
    //读线圈
    void on_r_coil_clicked();
    //读寄存器
    void on_r_regs_clicked();
    //清除窗口槽
    void on_clear_btn_clicked();
    //查看历史记录槽
    void on_check_history_clicked();
    void on_w_coil_clicked();

    void on_w_regs_clicked();

    void on_check_clicked();
    void on_tabWidget_tabBarClicked(int index);

private:

    Ui::MainWindow *ui;
    //串口线程对象
    uartWork *comThread;
    //串口配置结构体
    comcfg_t res_config;
    //串口开闭标志
    bool uartIsOpen=false;
    //串口数据缓冲
    QByteArray uartSrcData;

    //初始化超时重传次数
    int resendNumber=0;
    bool sendflag = false;
    //打开串口后禁止配置参数
    void uartConfigDisTrue();
    void uartConfigDisFalse();
    //写log信息到文件
    bool wirteQStringtoTxt(QString msg);
    //检测数据合法性
    bool checkUserInputData();
    bool checkUserInputWirteData();
    //询问判断
    bool judgement(QString msg);
    void closeEvent(QCloseEvent *event);
    void table_dataInit();
    void ReadOutIniData(void);
    void ReadOutIniDatac(int start);
    void ReadOutIniDatar(int start);
    //文件对象
    QFile *file = nullptr;
    //当前时间定时器对象
    QTimer * m_timer;
    //RTU报文封装类对象
    Modbusmsg * mb_msg;
    QTimer * m_resendtimer;
    quint8 bar_indexs = 0;
    QSettings* readseting=nullptr;
    //地址起始地址即数量信息
    QVector<quint16> inf;
    QByteArray coildata;
    QByteArray resenddata;
    QVector<quint16> regdata;
    //对话框窗口对象
    msgDialog * m_msg_dialog=nullptr;
    //0103数据输入窗口对象
    userInput0103Dialog * m_0103dialog=nullptr;
    //0f数据窗口对象
    userInputDialog0f * m_0fdialog=nullptr;
    //10数据据窗口对象
    userInputDialog10 * m_10dialog=nullptr;
    //功能码
    quint8 m_code=0xff;
};

#endif // MAINWINDOW_H

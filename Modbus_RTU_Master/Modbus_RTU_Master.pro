#-------------------------------------------------
#
# Project created by QtCreator 2021-12-23T14:37:09
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Modbus_RTU_Master
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    uartPort/uartwork.cpp \
    utils/commont.cpp \
    utils/crc.cpp \
    modbusmsg/modbusmsg.cpp \
    modbusmsg/msgdialog.cpp \
    inputdialog/userinput0103dialog.cpp \
    inputdialog/userinputdialog0f.cpp \
    inputdialog/userinputdialog10.cpp

HEADERS  += mainwindow.h \
    uartPort/uart_type.h \
    uartPort/uartwork.h \
    utils/commont.h \
    utils/crc.h \
    modbusmsg/modbusmsg.h \
    modbusmsg/msgdialog.h \
    inputdialog/userinput0103dialog.h \
    inputdialog/userinputdialog0f.h \
    inputdialog/userinputdialog10.h

FORMS    += mainwindow.ui \
    modbusmsg/msgdialog.ui \
    inputdialog/userinput0103dialog.ui \
    inputdialog/userinputdialog0f.ui \
    inputdialog/userinputdialog10.ui

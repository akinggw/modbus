#ifndef UART_TYPE_H
#define UART_TYPE_H

#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QMetaType>

typedef struct {
      qint32 baudRate;                   /* 波特率 */
      QSerialPort::DataBits dataBits;    /* 数据位 */
      QSerialPort::Parity paritys;       /* 校验位 */
      QSerialPort::StopBits stopBits;    /* 停止位 */
      QString portNum;                    /* 串口号 */
} comcfg_t;
Q_DECLARE_METATYPE(comcfg_t)
#endif // UART_TYPE_H

#ifndef UARTWORK_H
#define UARTWORK_H

#include <QByteArray>
#include <QDebug>
#include <QObject>
#include <QSerialPort>
#include <QThread>
#include <QTimer>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include"./uartPort/uart_type.h"

class uartWork : public QObject {
  Q_OBJECT
public:
  explicit uartWork();
  ~uartWork();

void errorOccurredqq(QSerialPort::SerialPortError error);
public slots:

  void startThread();
  void stopThread();
  void writeData(QByteArray);
  void updateComCfg(comcfg_t cfg);
private slots:
  void readyRead();
  // void errorString();
  void openUart();
  void timerOut();
signals:
  /* 打开成功发送 true,失败 false */
  void openError(bool);
  void readyread(QByteArray);
  void errOccurred(QString);

private:
  bool isConnectFlag;
  bool openFlag;
  QTimer *rcvDataTimer;
  QSerialPort *currSerial;
  QThread *currThread;
  QByteArray sendArray;
  comcfg_t currentCfg;
};

#endif // UARTWORK_H

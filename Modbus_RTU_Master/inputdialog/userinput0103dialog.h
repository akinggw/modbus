#ifndef USERINPUT0103DIALOG_H
#define USERINPUT0103DIALOG_H

#include <QDialog>
#include<QVector>
namespace Ui {
class userInput0103Dialog;
}

class userInput0103Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit userInput0103Dialog(QWidget *parent = 0);
    ~userInput0103Dialog();
    //设置数量范围限制
    void setNumLimited(QString text);
    //获取输入信息
    QVector<quint16> infdata();
private slots:
private:
    Ui::userInput0103Dialog *ui;
    QVector<quint16> inf;
};

#endif // USERINPUT0103DIALOG_H

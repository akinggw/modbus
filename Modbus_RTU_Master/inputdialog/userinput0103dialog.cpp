#include "userinput0103dialog.h"
#include "ui_userinput0103dialog.h"
#define READ_COIL_MINNUM 1  //请求报文读取线圈个数的最小值
#define READ_COIL_MAXNUM 2000  //请求报文读取线圈个数的最大值
#define READ_REGISTER_MINNUM 1 //请求报文读取寄存器个数的最小值
#define READ_REGISTER_MAXNUM 125 //请求报文读取寄存器个数的最大值
userInput0103Dialog::userInput0103Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::userInput0103Dialog)
{
    ui->setupUi(this);
}

userInput0103Dialog::~userInput0103Dialog()
{
    delete ui;
}

void userInput0103Dialog::setNumLimited(QString text)
{
    ui->numline->setPlaceholderText(text);
    if(text=="1-125")
    {
        ui->numline->setValidator(new QIntValidator(READ_REGISTER_MINNUM, READ_REGISTER_MAXNUM, this));
        ui->addrline->setValidator(new QIntValidator(1,247, this));
        ui->startaddrline->setValidator(new QIntValidator(0, 65535, this));
    }
    else if(text=="1-2000")
    {
        ui->numline->setValidator(new QIntValidator(READ_REGISTER_MINNUM, READ_COIL_MAXNUM, this));
        ui->addrline->setValidator(new QIntValidator(1,247, this));
        ui->startaddrline->setValidator(new QIntValidator(0, 65535, this));
    }
}

QVector<quint16> userInput0103Dialog::infdata()
{
    quint16 addr = ui->addrline->text().toInt();
    quint16 satrtaddr = ui->startaddrline->text().toInt();
    quint16 num = ui->numline->text().toInt();
    inf.push_back(addr);
    inf.push_back(satrtaddr);
    inf.push_back(num);
    return inf;
}

#ifndef USERINPUTDIALOG0F_H
#define USERINPUTDIALOG0F_H

#include <QDialog>
#include<QByteArray>
#include<QMessageBox>
#include<QStringList>
#include<QRegExp>
#include<QTableWidgetItem>
#include<QLineEdit>
#include<./utils/commont.h>
namespace Ui {
class userInputDialog0f;
}

class userInputDialog0f : public QDialog
{
    Q_OBJECT

public:
    explicit userInputDialog0f(QWidget *parent = 0);
    ~userInputDialog0f();
    //获取输入信息
    QVector<quint16> infdata();
    //获取用户输入数据
    QByteArray getUserInputData();
private slots:
    //设置完成
    void on_over_clicked();
    void on_start_btn_clicked();

    void on_tablecoil_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);

    void on_tablecoil_cellDoubleClicked(int row, int column);

    void on_check_clicked();

private:
    Ui::userInputDialog0f *ui;
    QVector<quint16> inf;
    //线圈表格初始化
    quint8 initdata[1968]={0};
    QByteArray bamsg;
    QRegExp rx;
    QString pre;
    quint16 oldstartaddr;
    int isfc=0;
    int isclicked=0;
    QLineEdit* edit;
    void inittable();
    void tabledatainit(quint16 BeginAddress,quint16 Number);
    void GetData0X01(QByteArray &coilsDataArr,quint16 BeginAddress,quint16 Number);
};

#endif // USERINPUTDIALOG0F_H

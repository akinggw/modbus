#include <QString>
#include <QtTest>
//测试依赖文件
#include<./utils/commont.h>
#include<./analysis/analysis_modbus.h>

class TCP_Salve_TestTest : public QObject
{
    Q_OBJECT

public:
    TCP_Salve_TestTest();
    analysis_modbus an_msg;
    quint8 value;

private Q_SLOTS:

    //测试读取状态执行对应函数
    void test_recvModbusMsg_data();
    void test_recvModbusMsg();
};

TCP_Salve_TestTest::TCP_Salve_TestTest()
{

}
void TCP_Salve_TestTest::test_recvModbusMsg_data()
{
    //ini读取对象
    QSettings readsetings("testini/dataTCPSalve.ini",QSettings::IniFormat);
    //创建测试表
    QTest::addColumn<QByteArray>("requestMessage");
    QTest::addColumn<quint8>("expected");
    //获取测试样例数据
    quint8 testnum = readsetings.value("TestCount/count").toInt();
    //获取所有测试样例
    for(quint8 i=0;i<testnum;i++)
    {
        QString requestMessagestrings = readsetings.value("section" + QString::number(i + 1) + "/reqMes").toString();
        quint8 expecteds =readsetings.value("section" + QString::number(i + 1) + "/except").toInt();

        QByteArray requestMessage;
        HexStringToByte(requestMessagestrings,requestMessage);
        //打印日志
        qDebug() << requestMessage << expecteds;

        QString rowcount = QString::number(i);

        QTest::newRow(rowcount.toUtf8().data())
                        << requestMessage
                        << expecteds
                           ;
    }
}

void TCP_Salve_TestTest::test_recvModbusMsg()
{
    QFETCH(QByteArray, requestMessage);
    QFETCH(quint8, expected);
    //调用测试函数
    value = (quint8)an_msg.recvModbusMsg(requestMessage);
    //断言
    QCOMPARE(value, expected);
}


QTEST_APPLESS_MAIN(TCP_Salve_TestTest)

#include "tst_tcp_salve_testtest.moc"

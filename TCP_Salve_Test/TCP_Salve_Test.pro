#-------------------------------------------------
#
# Project created by QtCreator 2022-01-06T14:55:42
#
#-------------------------------------------------

QT       += testlib
QT       += core gui
QT       += network

TARGET = tst_tcp_salve_testtest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tst_tcp_salve_testtest.cpp \
    analysis/analysis_modbus.cpp \
    utils/commont.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    analysis/analysis_modbus.h \
    utils/commont.h

#ifndef MODBUSMSG_H
#define MODBUSMSG_H

#include <QObject>
#include<QDebug>
#include<QSettings>
#include<QThread>
#include<QDateTime>
#include"./utils/commont.h"
#include"./utils/crc.h"
#define CRC_FLAG 0
#define ADDRESS_MIN 0  //线圈和寄存器的地址最小值
#define ADDRESS_MAX 65535  //线圈和寄存器的地址最大值
#define READ_COIL_MINNUM 1  //请求报文读取线圈个数的最小值
#define READ_COIL_MAXNUM 2000  //请求报文读取线圈个数的最大值
#define READ_REGISTER_MINNUM 1 //请求报文读取寄存器个数的最小值
#define READ_REGISTER_MAXNUM 125 //请求报文读取寄存器个数的最大值
#define WRITE_COIL_MINNUM 1  //请求报文写入线圈个数的最小值
#define WRITE_COIL_MAXNUM 1968  //请求报文写入线圈个数的最大值
#define WRITE_REGISTER_MINNUM 1 //请求报文写入寄存器个数的最小值
#define WRITE_REGISTER_MAXNUM 123 //请求报文写入寄存器个数的最大值

#define RTU_MESSAGE_MAX_BYTE 256 //RTU报文最大长度
#define MINIMUM_MESSAGE_LENGTH 8 //接收到的请求报文最小长度

#define REQUEST_MESSAGE_LENGTH_0X01_0X03 8 //0X01或0X03请求报文长度
#define ABNORMAL_RESPONSE_LENGTH 5  //异常响应报文长度
#define WRITE_RESPONSE_LENGTH 8 //正常写入响应报文长度
using namespace JQChecksum;
//数据长度
#define MAXDATA                256

class Modbusmsg : public QObject
{
    Q_OBJECT
public:

    explicit Modbusmsg();
    ~Modbusmsg();
    //0f10报文处理
    void packageSartc(quint8 addr,quint8 funcode,quint16 startaddr,quint16 num,QByteArray ba,QVector<quint16>regs);
    //0103报文处理
    void packageSartr(quint8 addr,quint8 funcode,quint16 startaddr,quint16 numreg);
    //回应报文解析
    bool ParseResponseMessage(QByteArray responseMessage);

    //帧长度
    quint8 mb_lenght=0;
    //报文封装缓冲
    quint8 msgbuf[MAXDATA];
    QVector<quint16> bar;
    QString coildata;
    //初始化请求报文数组
    QByteArray requestMessage;
    QSettings* readsetings=nullptr;
    QThread *m_thread;
    //报文封装函数
    //01功能码封装函数
    void funCode01(quint8 addr,quint16 startaddr,quint16 numreg);
    //03功能码封装函数
    void funCode03(quint8 addr,quint16 startaddr,quint16 numreg);
    //0f功能码封装函数
    void funCode0f(quint8 addr,quint16 startaddr,quint16 num,QByteArray ba);
    //10功能码封装函数
    void funCode10(quint8 addr,quint16 startaddr,QVector<quint16> addrvalue);

    void WirteRegsDataToIni(quint16 startaddr,quint16 num,QVector<quint16> ba);
    void WirteCoilDataToIni(quint16 satrt, QString CoilData);
    //0103报文解析处理
    bool RTU0X0103FuncCodeProcess(QByteArray responseMessage);
    //0f10报文解析处理
    bool RTU0X0f10FuncCodeProcess(QByteArray responseMessage);
    //异常功能码处理
    bool RTUExceptionCodeJudge(QByteArray responseMessage);

signals:
    void package_over(QByteArray package_msg);
    void showMsgtoUi(QString);
    void showlogmsg(QString);
    void wirtTablec(quint16 num, quint16 satrtaddr,QString bac);
    void wirtTabler(quint16 num, quint16 satrtaddr,QVector<quint16> ba);
public slots:
private:

};

#endif // MODBUSMSG_H

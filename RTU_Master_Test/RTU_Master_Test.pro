#-------------------------------------------------
#
# Project created by QtCreator 2022-01-06T10:09:34
#
#-------------------------------------------------

QT       += testlib
QT       += core gui
QT       += network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
TARGET = tst_rtu_master_testtest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tst_rtu_master_testtest.cpp \
    modbusmsg/modbusmsg.cpp \
    utils/commont.cpp \
    utils/crc.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    modbusmsg/modbusmsg.h \
    utils/commont.h \
    utils/crc.h

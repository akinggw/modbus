﻿#ifndef __CRC_H__
#define __CRC_H__

#include <QByteArray>

namespace JQChecksum
{
quint16 crc16ForModbus(const QByteArray &data,int flag);
}

#endif//__CRC_H__

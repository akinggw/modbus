#include <QString>
#include <QtTest>
#include <QCoreApplication>
#include<QSettings>
#include<QDebug>
#include<QRegExp>

//测试依赖文件
#include<./utils/commont.h>
#include<./utils/crc.h>
#include<./modbusmsg/modbusmsg.h>
using namespace JQChecksum;
class RTU_Master_TestTest : public QObject
{
    Q_OBJECT

public:
    RTU_Master_TestTest();
    Modbusmsg mb_msg;

private Q_SLOTS:

/*************************共用函数测试******************************/
       void CRCtest_data();
       void CRCtest();

    /************************测试数据*************************/
        //01功能码封装函数测试
        void funCode01_data();
        //03功能码封装函数测试
        void funCode03_data();
        //0f功能码封装函数测试
        void funCode0f_data();
        //10功能码封装函数测试
        void funCode10_data();

    /************************测试函数*************************/
        //01功能码封装函数
        void funCode01();
        //03功能码封装函数
        void funCode03();
        //0f功能码封装函数
        void funCode0f();
        //10功能码封装函数
        void funCode10();

    /************************回应报文解析函数测试*************************/
     void ParseResponseMessage_data();
     void ParseResponseMessage();


};

RTU_Master_TestTest::RTU_Master_TestTest()
{
}

/*******************************start******************************/
//CRC函数测试
void RTU_Master_TestTest::CRCtest_data()
{
    //ini读取对象
    QSettings readsetings("testini/CRCTest.ini",QSettings::IniFormat);
    //创建测试表
    QTest::addColumn<QByteArray>("data");
    QTest::addColumn<quint16>("expected");
    //获取测试样例数据
    quint8 testnum = readsetings.value("Section0/num").toInt();
    //获取所有测试样例
    for(quint8 i=0;i<testnum;i++)
    {
        quint16 nums = readsetings.value("Section" + QString::number(i + 1) + "/expected").toInt();
        QString expectedstrings = readsetings.value("Section" + QString::number(i + 1) + "/data").toString();

        QByteArray expecteds;
        HexStringToByte(expectedstrings,expecteds);
        //打印日志
        qDebug() << nums << expectedstrings;

        QString rowcount = QString::number(i);

        QTest::newRow(rowcount.toUtf8().data())
                        << expecteds
                        << nums;
    }

}
void RTU_Master_TestTest::CRCtest()
{
    QFETCH(QByteArray, data);
    QFETCH(quint16, expected);
    //调用测试函数
    quint16 crcvalue =crc16ForModbus(data,0);
    //断言
    QCOMPARE(crcvalue, expected);
}
/*******************************end******************************/

/*******************************start******************************/
//01功能码封装数据
void RTU_Master_TestTest::funCode01_data()
{
    //ini读取对象
    QSettings readsetings("testini/funCode01.ini",QSettings::IniFormat);
    //创建测试表
    QTest::addColumn<quint8>("addr");
    QTest::addColumn<quint16>("startAddr");
    QTest::addColumn<quint16>("num");
    QTest::addColumn<QByteArray>("expected");
    //获取测试样例数据
    quint8 testnum = readsetings.value("Section0/num").toInt();
    //获取所有测试样例
    for(quint8 i=0;i<testnum;i++)
    {
        quint8 addrs = readsetings.value("Section" + QString::number(i + 1) + "/addr").toInt();
        quint16 startAddrs = readsetings.value("Section" + QString::number(i + 1) + "/startAddr").toInt();
        quint16 nums = readsetings.value("Section" + QString::number(i + 1) + "/num").toInt();
        QString expectedstrings = readsetings.value("Section" + QString::number(i + 1) + "/expected").toString();
        QByteArray expecteds;
        HexStringToByte(expectedstrings,expecteds);
        //打印日志
        qDebug() << addrs << startAddrs << nums << expectedstrings;

        QString rowcount = QString::number(i);

        QTest::newRow(rowcount.toUtf8().data()) << addrs
                        << startAddrs
                        << nums
                        << expecteds;
    }
}
//01功能码封装函数测试
void RTU_Master_TestTest::funCode01()
{
    QFETCH(quint8, addr);
    QFETCH(quint16, startAddr);
    QFETCH(quint16, num);
    QFETCH(QByteArray, expected);
    //调用测试函数
    mb_msg.funCode01(addr,startAddr,num);
    //断言
    QCOMPARE(mb_msg.requestMessage, expected);
}
/*******************************end******************************/

/*******************************start******************************/
//03功能码封装数据
void RTU_Master_TestTest::funCode03_data()
{
    //ini读取对象
    QSettings readsetings("testini/funCode03.ini",QSettings::IniFormat);
    //创建测试表
    QTest::addColumn<quint8>("addr");
    QTest::addColumn<quint16>("startAddr");
    QTest::addColumn<quint16>("num");
    QTest::addColumn<QByteArray>("expected");
    //获取测试样例数据
    quint8 testnum = readsetings.value("Section0/num").toInt();
    //获取所有测试样例
    for(quint8 i=0;i<testnum;i++)
    {
        quint8 addrs = readsetings.value("Section" + QString::number(i + 1) + "/addr").toInt();
        quint16 startAddrs = readsetings.value("Section" + QString::number(i + 1) + "/startAddr").toInt();
        quint16 nums = readsetings.value("Section" + QString::number(i + 1) + "/num").toInt();
        QString expectedstrings = readsetings.value("Section" + QString::number(i + 1) + "/expected").toString();
        QByteArray expecteds;
        HexStringToByte(expectedstrings,expecteds);
        //打印日志
        qDebug() << addrs << startAddrs << nums << expectedstrings;

        QString rowcount = QString::number(i);

        QTest::newRow(rowcount.toUtf8().data()) << addrs
                        << startAddrs
                        << nums
                        << expecteds;
    }
}

//03功能码封装函数测试
void RTU_Master_TestTest::funCode03()
{
    QFETCH(quint8, addr);
    QFETCH(quint16, startAddr);
    QFETCH(quint16, num);
    QFETCH(QByteArray, expected);
    //调用测试函数
    mb_msg.funCode03(addr,startAddr,num);
    //断言
    QCOMPARE(mb_msg.requestMessage, expected);
}
/*******************************end******************************/

/*******************************start*****************************/
void RTU_Master_TestTest::funCode0f_data()
{
    //ini读取对象
    QSettings readsetings("testini/funCode0f.ini",QSettings::IniFormat);
    //创建测试表
    QTest::addColumn<quint8>("addr");
    QTest::addColumn<quint16>("startAddr");
    QTest::addColumn<quint16>("num");
    QTest::addColumn<QByteArray>("coildata");
    QTest::addColumn<QByteArray>("expected");
    //获取测试样例数据
    quint8 testnum = readsetings.value("Section0/num").toInt();
    //获取所有测试样例
    for(quint8 i=0;i<testnum;i++)
    {
        quint8 addrs = readsetings.value("Section" + QString::number(i + 1) + "/addr").toInt();
        quint16 startAddrs = readsetings.value("Section" + QString::number(i + 1) + "/startAddr").toInt();
        quint16 nums = readsetings.value("Section" + QString::number(i + 1) + "/num").toInt();
        QString coildatastrings = readsetings.value("Section" + QString::number(i + 1) + "/data").toString();
        QString expectedstrings = readsetings.value("Section" + QString::number(i + 1) + "/expected").toString();
        QByteArray expecteds,coildatas;
        HexStringToByte(coildatastrings,coildatas);
        HexStringToByte(expectedstrings,expecteds);
        //打印日志
        qDebug() << addrs << startAddrs << nums << expectedstrings << coildatastrings;

        QString rowcount = QString::number(i);

        QTest::newRow(rowcount.toUtf8().data()) << addrs
                        << startAddrs
                        << nums
                        << coildatas
                        << expecteds;
    }
}
void RTU_Master_TestTest::funCode0f()
{
    QFETCH(quint8, addr);
    QFETCH(quint16, startAddr);
    QFETCH(quint16, num);
    QFETCH(QByteArray, coildata);
    QFETCH(QByteArray, expected);
    //调用测试函数
    mb_msg.funCode0f(addr,startAddr,num,coildata);
    //断言
    QCOMPARE(mb_msg.requestMessage, expected);
}
/*******************************end*****************************/


/*******************************start*****************************/
void RTU_Master_TestTest::funCode10_data()
{
    //ini读取对象
    QSettings readsetings("testini/funCode10.ini",QSettings::IniFormat);
    //创建测试表
    QTest::addColumn<quint8>("addr");
    QTest::addColumn<quint16>("startAddr");
    QTest::addColumn<QVector<quint16>>("regsdata");
    QTest::addColumn<QByteArray>("expected");
    //获取测试样例数据
    quint8 testnum = readsetings.value("Section0/num").toInt();
    //获取所有测试样例
    for(quint8 i=0;i<testnum;i++)
    {
        quint8 addrs = readsetings.value("Section" + QString::number(i + 1) + "/addr").toInt();
        quint16 startAddrs = readsetings.value("Section" + QString::number(i + 1) + "/startAddr").toInt();
        quint16 nums = readsetings.value("Section" + QString::number(i + 1) + "/num").toInt();
        QString regdatastrings = readsetings.value("Section" + QString::number(i + 1) + "/data").toString();
        QString expectedstrings = readsetings.value("Section" + QString::number(i + 1) + "/expected").toString();
        QByteArray expecteds,rdatas;
        QVector<quint16> regdatas;
        HexStringToByte(regdatastrings,rdatas);
        for(quint8 i=0;i<rdatas.size();i++)
        {
            regdatas.push_back((quint16)rdatas.at(i));
        }
        HexStringToByte(expectedstrings,expecteds);
        //打印日志
        qDebug() << addrs << startAddrs << nums << expectedstrings << regdatastrings;

        QString rowcount = QString::number(i);

        QTest::newRow(rowcount.toUtf8().data()) << addrs
                        << startAddrs
                        << regdatas
                        << expecteds;
    }
}
void RTU_Master_TestTest::funCode10()
{
    QFETCH(quint8, addr);
    QFETCH(quint16, startAddr);
    QFETCH(QVector<quint16>, regsdata);
    QFETCH(QByteArray, expected);
    //调用测试函数
    mb_msg.funCode10(addr,startAddr,regsdata);
    //断言
    QCOMPARE(mb_msg.requestMessage, expected);
}


 /************************回应报文解析函数测试*************************/
void RTU_Master_TestTest::ParseResponseMessage_data()
{
    //ini读取对象
    QSettings readsetings("testini/ParseResponseMessage.ini",QSettings::IniFormat);
    //创建测试表
    QTest::addColumn<QByteArray>("requestMessage");
    QTest::addColumn<QByteArray>("responseMessage");
    QTest::addColumn<quint8>("expected");
    //获取测试样例数据
    quint8 testnum = readsetings.value("Section0/num").toInt();
    //获取所有测试样例
    for(quint8 i=0;i<testnum;i++)
    {
        QString requestMessagestrings = readsetings.value("Section" + QString::number(i + 1) + "/requestMessage").toString();
        QString responseMessagestrings = readsetings.value("Section" + QString::number(i + 1) + "/responseMessage").toString();
        quint8 expecteds =readsetings.value("Section" + QString::number(i + 1) + "/expected").toInt();

        QByteArray requestMessage,responseMessage;
        HexStringToByte(requestMessagestrings,requestMessage);
        HexStringToByte(responseMessagestrings,responseMessage);
        //打印日志
        qDebug() << requestMessage << responseMessage << expecteds;

        QString rowcount = QString::number(i);

        QTest::newRow(rowcount.toUtf8().data())
                        << requestMessage
                        << responseMessage
                        << expecteds
                           ;
    }
}

void RTU_Master_TestTest::ParseResponseMessage()
{
    QFETCH(QByteArray, requestMessage);
    QFETCH(QByteArray, responseMessage);
    QFETCH(quint8, expected);
    //调用测试函数
    mb_msg.requestMessage = requestMessage;
    quint8 value = (quint8)mb_msg.ParseResponseMessage(responseMessage);
    //断言
    QCOMPARE(value, expected);
}
/*******************************end*****************************/









QTEST_APPLESS_MAIN(RTU_Master_TestTest)

#include "tst_rtu_master_testtest.moc"
